import { Http, HttpModule, Headers, RequestOptions } from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject,BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class HttpService {
  GetUserInfo$: Observable<any>;
  private UserInfo = new Subject<any>();





  constructor(private http: Http , private router: Router) {
    this.GetUserInfo$ = this.UserInfo.asObservable();
    if (localStorage.getItem('favItem')) {
      const favDataLocal = JSON.parse(localStorage.getItem('favItem')!);
      this.favList = new Map(favDataLocal);
      this.favDataShare.next(this.favList);
    }

   }

   favList: Map<any, any> = new Map();
   // to shar fav and subscript
   favDataShare: BehaviorSubject<any> = new BehaviorSubject(null);

     // tslint:disable-next-line: typedef
    GetUserInfo(data: string) {
       // I have data! Let's return it so subscribers can use it!
        // we can do stuff with data if we want
        this.UserInfo.next(data);
    }



      // tslint:disable-next-line: typedef
  prepareRequestHeaders(){
    let headers: Headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Credentials', 'true');
    headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    return headers;
  }

  prepareRequestHeadersWithMultiPart(){
    let headers: Headers = new Headers();
    const user = JSON.parse(JSON.stringify(localStorage.getItem('token')));
    //headers.append('Content-Type', 'application/json');
    headers.append('encrypt', 'multipart/form-data');
    headers.append('Access-Control-Allow-Credentials', 'true');
    headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    headers.append('Authorization', 'Bearer ' + user);
    return headers;
  }


  // prepareRequestHeadersWithToken() {
  //   let headers: Headers = new Headers();
  //   const user = JSON.parse(JSON.stringify(localStorage.getItem('token')));
  //   headers.append('Content-Type', 'application/json');
  //   headers.append('Access-Control-Allow-Credentials', 'true');
  //   headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  //   headers.append('Access-Control-Allow-Origin', '*');
  //   headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  //   headers.append('Authorization', 'Bearer ' + user);
  //   return headers;
  // }



  // tslint:disable-next-line: typedef
  prepareRequestHeadersWithToken() {
    let headers: Headers = new Headers();
    const user = JSON.parse(JSON.stringify(localStorage.getItem('token')));
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Credentials', 'true');
    headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    headers.append('Authorization', 'Bearer ' + user);
    return headers;
  }


  // tslint:disable-next-line: typedef
  POSTWithoutToken(url: any, data: any) {
    return this.http.post(url, data, { headers: this.prepareRequestHeaders() })
      .pipe(
        map(res => res.json()),

      );
  }


  POSTWithoutTokenMultiPart(url: any, data: any) {
    return this.http.post(url, data, { headers: this.prepareRequestHeadersWithMultiPart() })
      .pipe(
        map(res => res.json()),

      );
  }


  // tslint:disable-next-line: typedef
  POSTWithTokenAndParam(url: any, data: any)
  {
    return this.http.post(url, data, { headers: this.prepareRequestHeadersWithToken() })
      .pipe(
        map(res => res.json()),

      );
  }

  // tslint:disable-next-line: typedef
  POST(url: any, data: any) {
    return this.http.post(url, data, { headers: this.prepareRequestHeadersWithToken() })
      .pipe(
        map(res => res.json()),

      );

  }

  // tslint:disable-next-line: typedef
  Put(url: any, Data: any)
  {
    return this.http.put(url, Data, { headers: this.prepareRequestHeadersWithToken() })
      .pipe(
        map(res => res.json()),
      );
  }


  PUTWithoutTokenMultiPart(url: any, data: any)
  {
    return this.http.put(url, data, { headers: this.prepareRequestHeadersWithMultiPart() })
      .pipe(
        map(res => res.json()),

      );
  }

  /////useeeeeeeeeeeeeeeed
  // tslint:disable-next-line: typedef
  GetWithoutToken(url: any){
    return this.http.get(url)
    .pipe(
      map(res => res.json()),
    );
  }
    /////useeeeeeeeeeeeeeeed
  GetWithoutTokenwithparams(url: any, Params: any){
    return this.http.get(url,{params: Params})
    .pipe(
      map(res => res.json()),
    );
  }

  // tslint:disable-next-line: typedef
  GET(url: any) {

    return this.http.get(url, { headers: this.prepareRequestHeadersWithToken() })
      .pipe(
        map(res => res.json()),
      );
  }
  // tslint:disable-next-line: typedef
  GETWithParams(url: any, Params: any) {

    return this.http.get(url, {params: Params, headers: this.prepareRequestHeadersWithToken() })
      .pipe(
        map(res => res.json()),
      );
  }

  async GETWithParamsWithPromis(url: any, Params: any) {

    return await this.http.get(url, {params: Params, headers: this.prepareRequestHeadersWithToken() })
      .pipe(
        map(res => res.json()),
      );
  }

  // tslint:disable-next-line: typedef
  Delete(url: any, data: any)
  {
    return this.http.delete(url, { params: data, headers: this.prepareRequestHeadersWithToken() })
    .pipe(
      map(res => res.json()),
    );
  }

  DeleteWithParams(url: any, Params: any) {

    return this.http.delete(url, {params: Params, headers: this.prepareRequestHeadersWithToken() })
      .pipe(
        map(res => res.json()),
      );
  }

  // tslint:disable-next-line: typedef
  getToken(){
    return localStorage.getItem('token');
  }
    // tslint:disable-next-line: typedef
  isLoggedIn() {
    return !!localStorage.getItem('token');
  }



  isAdmin(){
    if(localStorage.getItem('token') != null && (localStorage.getItem('roles') == "Admin") )
    {
      return true;
    }
    else
   {
    return false;
   }
  }

}
