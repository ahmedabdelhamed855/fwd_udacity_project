import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {HttpService} from './http.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardForAdmin implements CanActivate {
  // tslint:disable-next-line: variable-name
  constructor( private _Router: Router,
    private _Auth: HttpService){}
    canActivate(): boolean{
        if (this._Auth.isAdmin()){
          return true;
        }
        else{
          this._Router.navigate(['/']);
          return false;
        }
    }
}
