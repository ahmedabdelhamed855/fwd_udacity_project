import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { getAllProduct } from 'src/app/config/Apis';
import { HttpService } from 'src/app/Services/http.service';
import { Product } from 'src/app/Shared/Models/product';
import * as AOS from 'aos';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  alert:boolean= false;
  alert2:boolean= false;
  productList:Product[]=[];
  cartData: any[] = [];
  count:number=1;
  favList: Map<any, any> = new Map();
  countList: Map<any, any> = new Map();
  titleForAlert:string=''
  constructor(private httpService: HttpService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit(): void {

    //get all data from api abd push it in productList
    this.httpService.GetWithoutToken(getAllProduct).subscribe(
      res=>{
        this.productList=res.products;
        console.log(this.productList)
      }
    )

    // this to load the favList and push on it if this list has items.
    const favDataLocal = JSON.parse(localStorage.getItem('favItem')!);
    this.favList = new Map(favDataLocal);
    console.log(this.favList);
    AOS.init();
  }


  routee(i:number){
    debugger;
    this.router.navigate(['details'],{queryParams : {Id : i}});
  }

  // fuction to get the number of item
  numberSelected(event:any){

    if(Number(event.target.value)==0){
      this.count=1;
    }
    else{
      this.count=Number(event.target.value);
    }

  }

  // to add fav in cart and make alert if add or not
  addFav(data: any): void {
    debugger
    if (!this.favList.has(data.id)) {
      this.favList.set(data.id,data);
      data.discountPercentage=this.count;
      this.savData();
      this.count=1;
      // this is to add the name to alert in html
      this.titleForAlert=data.title;
      this.alert2=true;
    }
    else{
      // this is to add the name to alert in html
      this.titleForAlert=data.title;
      this.alert=true;
    }

  }

  // to save food fav to local
  savData(): void {
    const getValues = [...this.favList];
    localStorage.setItem('favItem', JSON.stringify(getValues));
    this.httpService.favDataShare.next(this.favList);
  }

  // fuction for close alert
  closeAlert(){
    this.alert=false;
    this.alert2=false
  }

}
