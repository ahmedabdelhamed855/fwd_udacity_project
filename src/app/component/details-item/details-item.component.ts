import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { getAllProduct } from 'src/app/config/Apis';
import { HttpService } from 'src/app/Services/http.service';
import { Product } from './../../Shared/Models/product';

@Component({
  selector: 'app-details-item',
  templateUrl: './details-item.component.html',
  styleUrls: ['./details-item.component.css']
})
export class DetailsItemComponent implements OnInit {

  alert:boolean= false;
  alert2:boolean= false;
  productList:Product[]=[];
  count:number=1;
  favList: Map<any, any> = new Map();
  titleForAlert:string=''

  constructor(private httpService: HttpService,private router: Router,private route: ActivatedRoute,private cdRef: ChangeDetectorRef) { }
  SelectedItemId : number | any;
  model:Product=new Product(0,'','','',0,'','',0,0,0);
  dataList: any;

  ngOnInit(): void {
    //to get the id of item that selected to see details
    this.route.queryParams.subscribe(param =>{
      this.SelectedItemId = param["Id"];
    })
    //to catch the data of this id
    this.httpService.GetWithoutTokenwithparams(getAllProduct+"/"+this.SelectedItemId,this.SelectedItemId).subscribe(
      res=>{
        this.model=res;
      }
    )
    // to get all data from local storage and apear them
    this.httpService.favDataShare.subscribe(() => {
      this.dataList = this.httpService.favDataShare.getValue()
        ? [...this.httpService.favDataShare.getValue().values()]
        : '';
      this.cdRef.detectChanges();
    });
    const favDataLocal = JSON.parse(localStorage.getItem('favItem')!);
    this.favList = new Map(favDataLocal);

  }



  numberSelected(event:any){

    if(Number(event.target.value)==0){
      this.count=1;
    }
    else{
      this.count=Number(event.target.value);

    }
  }


  // to add fav in cart
  addFav(data: any): void {
    if (!this.favList.has(data.id)) {
      this.favList.set(data.id,data);
      data.discountPercentage=this.count;
      this.savData();
      this.count=1;
      // this is to add the name to alert in html
      this.titleForAlert=data.title;
      this.alert2=true;
    }
    else{
      this.titleForAlert=data.title;
      this.alert=true;
    }
  }

  // to save food fav to local
  savData(): void {
    const getValues = [...this.favList];
    localStorage.setItem('favItem', JSON.stringify(getValues));
    this.httpService.favDataShare.next(this.favList);
  }

 //function for close the alerr
  closeAlert(){
    this.alert=false;
    this.alert2=false
  }

}
