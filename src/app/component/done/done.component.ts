import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-done',
  templateUrl: './done.component.html',
  styleUrls: ['./done.component.css']
})
export class DoneComponent implements OnInit {

  user = JSON.parse(JSON.stringify(localStorage.getItem('userName')));
  totalResult = JSON.parse(JSON.stringify(localStorage.getItem('totalResult')));
  constructor() { }

  ngOnInit(): void {
  }

}
