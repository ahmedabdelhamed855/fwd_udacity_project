import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from 'src/app/Services/http.service';
import { formDto } from 'src/app/Shared/Models/formDto';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  constructor(private httpService: HttpService,private router: Router,private route: ActivatedRoute,private cdRef: ChangeDetectorRef) { }
  dataList: any;
  newItemCount :number=0;
  totalResult:number=0;
  resultForItem:number=0
  model:formDto=new formDto('','',0);


  ngOnInit(): void {
    // to get all data from local storage and apear them
    this.httpService.favDataShare.subscribe(() => {
      this.dataList = this.httpService.favDataShare.getValue()
        ? [...this.httpService.favDataShare.getValue().values()]
        : '';
      this.cdRef.detectChanges();
    });
    // fuction to calculate the total price of item
    this.dataList.forEach((element: { price: number; discountPercentage: number; }) => {
      this.resultForItem=element.price*element.discountPercentage;
      this.totalResult=this.totalResult+this.resultForItem;
    });
  }

  // to recalculate the total price agin if user change the number of fav item in favitems list
  changeNumber(){
    this.totalResult=0;
    this.dataList.forEach((element: { price: number; discountPercentage: number; }) => {
      this.resultForItem=element.price*element.discountPercentage;
      this.totalResult=this.totalResult+this.resultForItem;
    });
    localStorage.setItem('totalResult',JSON.stringify(this.totalResult));
  }

  onSubmit(form: NgForm){
    //console.log(form);
    // catch el data from the form
    this.model.Name=form.value.Name;
    this.model.Address=form.value.Address;
    this.model.CardNumber=form.value.CardNumber;

    localStorage.setItem('userName',this.model.Name);

    this.router.navigate(['done']);
  }

  removeItem(idProduct:number){
    this.dataList.splice(this.dataList.indexOf(this.dataList.filter((x: { id: number; }) => x.id == idProduct)[0]),1);
    this.totalResult=0;
    this.dataList.forEach((element: { price: number; discountPercentage: number; }) => {
      this.resultForItem=element.price*element.discountPercentage;
      this.totalResult=this.totalResult+this.resultForItem;
    });
    localStorage.setItem('totalResult',JSON.stringify(this.totalResult));
  }

}
