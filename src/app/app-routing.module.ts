import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartComponent } from './component/cart/cart.component';
import { DetailsItemComponent } from './component/details-item/details-item.component';
import { DoneComponent } from './component/done/done.component';
import { HomeComponent } from './component/home/home.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'details',component:DetailsItemComponent},
  {path:'cart',component:CartComponent},
  {path:'done',component:DoneComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
