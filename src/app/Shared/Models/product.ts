export class Product{
  id:number;
  title:string;
  description:string;
  thumbnail:string;
  price:number;
  category:String;
  brand:string;
  discountPercentage:number;
  rating:number;
  stock:number;
  images:string[]=[];


  constructor(
    id:number,title:string,description:string,thumbnail:string,price:number,category:string,brand:string,discountPercentage:number,rating:number,stock:number
    )
    {
      this.id=id;
      this.title=title;
      this.description=description;
      this.thumbnail=thumbnail;
      this.price=price;
      this.category=category;
      this.brand=brand;
      this.discountPercentage=discountPercentage;
      this.rating=rating;
      this.stock=stock;
    }
}
