# FWDProject

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.5.

-FWD Project For E-commerce for Online shopping .
-Use Fake Api to fetch data .
-using effect(simple animation),Alert,form,validation,multi Component,share data , local storage.
-using npm i .
-using ng s --o to start the project .

General functionality:
  -GET And display items for shopping.
  -GET item details to show the describtion for this item that you selected.
  -Add item for shopping card.
  -Can control the count of item that you before and after adding in cart.
  -Calculate the total price of items in cart.
  -Delete any item you need from the cart.
  -POST Name ,Address and Credit Card number from form.
  -Finally Screen for Success with Your Name , Total price and shipping days.

The general page breakdown looks like this:
  -NAVBAR:
    * You Can Navigate bettween home to cart.
  -Home Page (url:''):
    * GET And display items for shopping.
    * Add item for shopping card.

  -Details Page(url:'/details'):
    * GET And display the product by id that you selected from home page.
    * Add item for shopping card.

  CART Page(url:'/cart'):
    * GET And display the list of fav items that added from home and details page.
    * DELETE from items from list of fav items.
    * CALCULATE the total price fo fav list items.
    * POST name , address and credit card number from form .

  -DONE Page(url:'/done'):
    * Finally Screen for Success with Your Name , Total price and shipping days.
  

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
